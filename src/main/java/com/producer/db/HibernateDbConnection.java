package com.producer.db;

import java.util.Properties;

public class HibernateDbConnection {

    public static Properties hibernateH2(){
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        properties.setProperty("hibernate.connection.url", "jdbc:h2:/Users/mauriciobento/data/test;AUTO_SERVER=TRUE");
        properties.setProperty("hibernate.connection.username", "sa");
        properties.setProperty("hibernate.connection.password", "");
        properties.setProperty("hibernate.hbm2ddl.auto", "create");
        properties.setProperty("show_sql", "false");
        return properties;
    }
}

