package com.producer.rest;

import com.producer.service.ProducerServiceAsync;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Iterator;

@Path("/producer")
public class ProducerService {

	static Logger logger = Logger.getLogger(ProducerService.class);

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response produceMessage(String msg) {
		logger.info("Message received: "+msg);

		final ProducerServiceAsync producerServiceAsync = new ProducerServiceAsync(msg);
		Runnable producerAsync = new Runnable() {
			@Override
			public void run() {
				producerServiceAsync.send();
			}
		};
		Thread producerThread = new Thread(producerAsync);
		producerThread.start();

		String output = "Message produced: "+msg;
		return Response.status(200).entity(output).build();
	}

	private HashMap<String, String> jsonToMap(String t) {
		HashMap<String, String> map = new HashMap<String, String>();
		JSONObject jObject = new JSONObject(t);
		Iterator<?> keys = jObject.keys();
		while( keys.hasNext() ){
			String key = (String)keys.next();
			String value = jObject.getString(key);
			map.put(key, value);

		}

		System.out.println("json : "+jObject);
		System.out.println("map : "+map);
		return map;
	}

}
