package com.producer.service;

import com.producer.db.HibernateUtil;
import com.producer.entity.ProduceMessage;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.UUID;

public class ProducerServiceAsync {

    static Logger logger = Logger.getLogger(ProducerServiceAsync.class);

    private String msg;

    public ProducerServiceAsync(String msg){
        this.msg = msg;
    }

    public void send() {

        ProduceMessage produceMessage = new ProduceMessage();
        produceMessage.setTransactionId(UUID.randomUUID().toString());
        produceMessage.setMessage(msg);
        produceMessage.setConsumed(false);

        if(msg.contains("BYUI")){
            produceMessage.setMessageType("BYUI");
        }else if(msg.contains("Pizza Hut")){
            produceMessage.setMessageType("Pizza Hut");
        }

        logger.info("Sending the message: " + produceMessage);

        try{
            SessionFactory sf = HibernateUtil.getSessionFactory();
            Session s = sf.openSession();
            Transaction transaction = s.beginTransaction();
            s.save(produceMessage);
            transaction.commit();
            s.close();
        }catch (Exception e){
            logger.error("Error to send the message: " + produceMessage);
        }
    }
}
