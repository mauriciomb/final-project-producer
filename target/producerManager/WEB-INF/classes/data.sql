DROP TABLE IF EXISTS produce_message;

CREATE TABLE produce_message (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  message VARCHAR(250) NOT NULL,
  messageType VARCHAR(250) NOT NULL
);
